"""Implements a generic training loop.
"""
import os
import sys
# from torch.utils.tensorboard import SummaryWriter
import time


import torch
import numpy as np
from tqdm.autonotebook import tqdm

import utils.diff_operators
import modules
import utils.utils as utils
from utils.loss_functions import mse_loss
import utils.diff_operators as diff_operators


def train_quadembs_mm(
    models, train_dataloader, epochs, steps_til_summary, optim, epochs_decaylr=8000
):
    """This definition is tailored for Miches-Menten model.

    Args:
        models (dict): it contains encoding, decoding, and quadratic models.
        train_dataloader (dataloader): it contains training data
        epochs (int): number of epochs
        steps_til_summary (int): printing summary after this many epochs
        optim (optim): optimizer
        epochs_decaylr (int, optional): decay learning rate after this many epcohs by factor of 10. Defaults to 8000.

    Returns:
        (models, loss): training models and history of loss function.
    """

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    scheduler = torch.optim.lr_scheduler.StepLR(
        optim, step_size=epochs_decaylr * len(train_dataloader), gamma=0.1
    )

    with tqdm(total=len(train_dataloader) * epochs) as pbar:
        losses = {
            "TotalLoss": [],
            "EncDec": [],
            "Deri_zxt": [],
            "Deri_xzt": [],
            "L1_loss": [],
        }

        for epoch in range(epochs):
            epoch_loss, epoch_loss_EncDec, epoch_loss_deri_zxt, epoch_loss_deri_xzt, = (
                0.0,
                0.0,
                0.0,
                0.0,
            )

            for step, (model_input, deri_output) in enumerate(train_dataloader):
                start_time = time.time()

                model_input = {
                    key: value.to(device) for key, value in model_input.items()
                }
                deri_output = {
                    key: value.to(device) for key, value in deri_output.items()
                }

                #
                enc_output = models["encoder"](
                    model_input
                )  # determining encoding (x -> z)
                dec_output = models["decoder"](
                    enc_output["model_out"]
                )  # determinig x from encoding (z -> x)
                dz_quad = models["quadModel"](
                    enc_output["model_out"]
                )  # Computing derivative of z as quadratic function

                # Computing derivative of z using automatic differentiation (i.e., (dz/dx)* (dx/dt))
                d_zt = []
                for i in range(enc_output["model_out"].shape[-1]):
                    tmp_dzx = diff_operators.gradient(
                        enc_output["model_out"][..., i], enc_output["model_in"]
                    )
                    tmp_dzxt = utils.dot_product(
                        tmp_dzx, deri_output["deri_info"]
                    )
                    d_zt.append(tmp_dzxt)

                d_zt = torch.cat(d_zt, dim=1)

                # Computing derivative of x again using automatic differentiation (i.e., (dx/dz)* (dz/dt))
                d_xt = []
                for i in range(dec_output["model_out"].shape[-1]):
                    tmp_dxz = diff_operators.gradient(
                        dec_output["model_out"][..., i], dec_output["model_in"]
                    )
                    tmp_dxzt = utils.dot_product(
                        tmp_dxz, dz_quad["model_out"]
                    )
                    d_xt.append(tmp_dxzt)

                d_xt = torch.cat(d_xt, dim=1)

                # Computing losses
                train_loss = 0.0

                tmp_loss_EncDec = mse_loss(
                    dec_output["model_out"], model_input["coords"]
                )  # Enc_Dec loss
                epoch_loss_EncDec += tmp_loss_EncDec
                train_loss += tmp_loss_EncDec

                tmp_loss_deri_xzt = mse_loss(d_xt, deri_output["deri_info"])
                epoch_loss_deri_xzt += tmp_loss_deri_xzt
                train_loss += tmp_loss_deri_xzt

                tmp_loss_deri_zxt = mse_loss(dz_quad["model_out"], d_zt)
                epoch_loss_deri_zxt += tmp_loss_deri_zxt
                train_loss += tmp_loss_deri_zxt

                # l1-loss
                l1_loss1 = models["quadModel"].fc.weight.abs().mean()
                l1_loss2 = models["decoder"].fc.weight.abs().mean()

                L1_PARAM = 1e-5

                l1_loss = L1_PARAM * (l1_loss1)
                train_loss += l1_loss

                epoch_loss += train_loss

                # backprop
                optim.zero_grad()
                train_loss.backward()
                optim.step()
                pbar.update(1)
                scheduler.step()

            losses["TotalLoss"].append(epoch_loss.item())
            losses["EncDec"].append(epoch_loss_EncDec.item())
            losses["Deri_xzt"].append(epoch_loss_deri_xzt.item())
            losses["Deri_zxt"].append(epoch_loss_deri_zxt.item())
            losses["L1_loss"].append(l1_loss.item())

            losses_str = " ".join(
                ["%s:%.2e" % (key, value[-1]) for key, value in losses.items()]
            )

            if not (epoch + 1) % steps_til_summary:
                tqdm.write(
                    "Epoch %d, Losses: %s iteration time %0.2f, lr %0.2e %0.2e"
                    % (
                        epoch,
                        losses_str,
                        time.time() - start_time,
                        optim.param_groups[0]["lr"],
                        optim.param_groups[1]["lr"],
                    )
                )

    return models, losses


def train_quadembs_pendulum(
    models, train_dataloader, epochs, steps_til_summary, optim, epochs_decaylr=8000
):
    """This definition is tailored for Miches-Menten model.

    Args:
        models (dict): it contains encoding, decoding, and quadratic models.
        train_dataloader (dataloader): it contains training data
        epochs (int): number of epochs
        steps_til_summary (int): printing summary after this many epochs
        optim (optim): optimizer
        epochs_decaylr (int, optional): decay learning rate after this many epcohs by factor of 10. Defaults to 8000.

    Returns:
        (models, loss): training models and history of loss function.
    """

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    scheduler = torch.optim.lr_scheduler.StepLR(
        optim, step_size=epochs_decaylr * len(train_dataloader), gamma=0.1
    )

    with tqdm(total=len(train_dataloader) * epochs) as pbar:
        losses = {
            "TotalLoss": [],
            "EncDec": [],
            "Deri_zxt": [],
            "Deri_xzt": [],
            "L1_loss": [],
        }

        for epoch in range(epochs):
            epoch_loss, epoch_loss_EncDec, epoch_loss_deri_zxt, epoch_loss_deri_xzt, = (
                0.0,
                0.0,
                0.0,
                0.0,
            )

            for step, (model_input, deri_output) in enumerate(train_dataloader):
                start_time = time.time()

                model_input = {
                    key: value.to(device) for key, value in model_input.items()
                }
                deri_output = {
                    key: value.to(device) for key, value in deri_output.items()
                }

                #
                enc_output = models["encoder"](
                    model_input
                )  # determining encoding (x -> z)
                dec_output = models["decoder"](
                    enc_output["model_out"]
                )  # determinig x from encoding (z -> x)
                dz_quad = models["quadModel"](
                    enc_output["model_out"]
                )  # Computing derivative of z as quadratic function

                # Computing derivative of z using automatic differentiation (i.e., (dz/dx)* (dx/dt))
                d_zt = []
                for i in range(enc_output["model_out"].shape[-1]):
                    tmp_dzx = diff_operators.gradient(
                        enc_output["model_out"][..., i], enc_output["model_in"]
                    )
                    tmp_dzxt = utils.dot_product(
                        tmp_dzx, deri_output["deri_info"]
                    )
                    d_zt.append(tmp_dzxt)

                d_zt = torch.cat(d_zt, dim=1)

                # Computing derivative of x again using automatic differentiation (i.e., (dx/dz)* (dz/dt))
                d_xt = []
                for i in range(dec_output["model_out"].shape[-1]):
                    tmp_dxz = diff_operators.gradient(
                        dec_output["model_out"][..., i], dec_output["model_in"]
                    )
                    tmp_dxzt = utils.dot_product(
                        tmp_dxz, dz_quad["model_out"]
                    )
                    d_xt.append(tmp_dxzt)

                d_xt = torch.cat(d_xt, dim=1)

                # Computing losses
                train_loss = 0.0

                tmp_loss_EncDec = mse_loss(
                    dec_output["model_out"], model_input["coords"]
                )  # Enc_Dec loss
                epoch_loss_EncDec += tmp_loss_EncDec
                train_loss += tmp_loss_EncDec

                tmp_loss_deri_xzt = mse_loss(d_xt, deri_output["deri_info"])
                epoch_loss_deri_xzt += tmp_loss_deri_xzt
                train_loss += tmp_loss_deri_xzt

                tmp_loss_deri_zxt = mse_loss(dz_quad["model_out"], d_zt)
                epoch_loss_deri_zxt += tmp_loss_deri_zxt
                train_loss += tmp_loss_deri_zxt

                # l1-loss
                l1_loss1 = models["quadModel"].fc.weight.abs().mean()
                l1_loss2 = models["decoder"].fc.weight.abs().mean()

                L1_PARAM = 1e-4

                l1_loss = L1_PARAM * (l1_loss1)
                train_loss += l1_loss

                epoch_loss += train_loss

                # backprop
                optim.zero_grad()
                train_loss.backward()
                optim.step()
                pbar.update(1)
                scheduler.step()

            losses["TotalLoss"].append(epoch_loss.item())
            losses["EncDec"].append(epoch_loss_EncDec.item())
            losses["Deri_xzt"].append(epoch_loss_deri_xzt.item())
            losses["Deri_zxt"].append(epoch_loss_deri_zxt.item())
            losses["L1_loss"].append(l1_loss.item())

            losses_str = " ".join(
                ["%s:%.2e" % (key, value[-1]) for key, value in losses.items()]
            )

            if not (epoch + 1) % steps_til_summary:
                tqdm.write(
                    "Epoch %d, Losses: %s iteration time %0.2f, lr %0.2e %0.2e"
                    % (
                        epoch,
                        losses_str,
                        time.time() - start_time,
                        optim.param_groups[0]["lr"],
                        optim.param_groups[1]["lr"],
                    )
                )

    return models, losses


def train_quademds_RD(
    models,
    train_dataloader,
    epochs,
    steps_til_summary,
    optim,
    epochs_decaylr=8000,
):
    """This definition is tailored for Reaction-Diffusion model.

    Args:
        models (dict): it contains encoding, decoding, and quadratic models.
        train_dataloader (dataloader): it contains training data
        epochs (int): number of epochs
        steps_til_summary (int): printing summary after this many epochs
        optim (optim): optimizer
        epochs_decaylr (int, optional): decay learning rate after this many epcohs by factor of 10. Defaults to 8000.

    Returns:
        (models, loss): training models and history of loss function.
    """

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    scheduler = torch.optim.lr_scheduler.StepLR(
        optim, step_size=epochs_decaylr * len(train_dataloader), gamma=0.1
    )

    with tqdm(total=len(train_dataloader) * epochs) as pbar:
        losses = {
            "TotalLoss": [],
            "EncDec": [],
            "Deri_zxt": [],
            "Deri_xzt": [],
            "L1_loss": [],
        }

        for epoch in range(epochs):
            epoch_loss, epoch_loss_EncDec, epoch_loss_deri_zxt, epoch_loss_deri_xzt, = (
                0.0,
                0.0,
                0.0,
                0.0,
            )

            for step, (model_input, deri_output) in enumerate(train_dataloader):
                start_time = time.time()

                model_input = {
                    key: value.to(device) for key, value in model_input.items()
                }
                deri_output = {
                    key: value.to(device) for key, value in deri_output.items()
                }

                #
                enc_output = models["encoder"](
                    model_input
                )  # determining encoding (x -> z)
                dec_output = models["decoder"](
                    enc_output["model_out"]
                )  # determinig x from encoding (z -> x)
                dz_quad = models["quadModel"](
                    enc_output["model_out"]
                )  # Computing derivative of z as quadratic function

                # Computing derivative of z using automatic differentiation (i.e., (dz/dx)* (dx/dt))
                d_zt = []
                for i in range(enc_output["model_out"].shape[-1]):
                    tmp_dzx = diff_operators.gradient(
                        enc_output["model_out"][..., i], enc_output["model_in"]
                    )
                    tmp_dzxt = utils.dot_product(
                        tmp_dzx, deri_output["deri_info"]
                    )
                    d_zt.append(tmp_dzxt)

                d_zt = torch.cat(d_zt, dim=1)

                # Computing derivative of x again using automatic differentiation (i.e., (dx/dz)* (dz/dt))
                d_xt = []
                for i in range(dec_output["model_out"].shape[-1]):
                    tmp_dxz = diff_operators.gradient(
                        dec_output["model_out"][..., i], dec_output["model_in"]
                    )
                    tmp_dxzt = utils.dot_product(
                        tmp_dxz, dz_quad["model_out"]
                    )
                    d_xt.append(tmp_dxzt)

                d_xt = torch.cat(d_xt, dim=1)

                # Computing losses
                train_loss = 0.0

                tmp_loss_EncDec = mse_loss(
                    dec_output["model_out"], model_input["coords"]
                )  # Enc_Dec loss
                epoch_loss_EncDec += tmp_loss_EncDec
                train_loss += tmp_loss_EncDec

                tmp_loss_deri_xzt = mse_loss(d_xt, deri_output["deri_info"])
                epoch_loss_deri_xzt += tmp_loss_deri_xzt
                train_loss += tmp_loss_deri_xzt

                tmp_loss_deri_zxt = mse_loss(dz_quad["model_out"], d_zt)
                epoch_loss_deri_zxt += tmp_loss_deri_zxt
                train_loss += tmp_loss_deri_zxt

                # l1-loss
                l1_loss1 = models["quadModel"].fc.weight.abs().mean()
                l1_loss2 = models["decoder"].fc.weight.abs().mean()

                L1_PARAM = 1e-4

                l1_loss = L1_PARAM * (l1_loss1)
                train_loss += l1_loss

                epoch_loss += train_loss

                # backprop
                optim.zero_grad()
                train_loss.backward()
                optim.step()
                pbar.update(1)
                scheduler.step()

            losses["TotalLoss"].append(epoch_loss.item())
            losses["EncDec"].append(epoch_loss_EncDec.item())
            losses["Deri_xzt"].append(epoch_loss_deri_xzt.item())
            losses["Deri_zxt"].append(epoch_loss_deri_zxt.item())
            losses["L1_loss"].append(l1_loss.item())

            losses_str = " ".join(
                ["%s:%.2e" % (key, value[-1]) for key, value in losses.items()]
            )

            if not (epoch + 1) % steps_til_summary:
                tqdm.write(
                    "Epoch %d, Losses: %s iteration time %0.2f, lr %0.2e %0.2e"
                    % (
                        epoch,
                        losses_str,
                        time.time() - start_time,
                        optim.param_groups[0]["lr"],
                        optim.param_groups[1]["lr"],
                    )
                )

    return models, losses
