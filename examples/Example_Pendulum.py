""" This is an example of pendulum!"""
# pylint: disable=E1101
# #!/usr/bin/env python
# coding: utf-8

import os
import sys
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
import torch
from scipy.integrate import solve_ivp
from torch.utils.data import DataLoader, Dataset

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ["KMP_DUPLICATE_LIB_OK"] = "True"

import utils.dynamical_models as dynamical_models
import modules
import utils.utils as utils
from functions.training import train_quadembs_pendulum

# Plotting parameter setting
SIZE = 20
params = {
    "legend.fontsize": "large",
    "figure.figsize": (20, 8),
    "axes.labelsize": SIZE,
    "axes.titlesize": SIZE,
    "xtick.labelsize": SIZE * 0.75,
    "ytick.labelsize": SIZE * 0.75,
    "axes.titlepad": 25,
}
plt.rcParams.update(params)

# For reproducbility
SEED = 42
utils.reproducibility_seed(SEED)


@dataclass
class ParametersPendulum:
    """Defining importart parameters."""

    model = "selu"  # Activation function
    mode = "mlp"  # Muti-layer perceptron
    hidden_features = 64  # Number of neurons
    num_hidden_layers = 3  # Number of layers
    dim_x = 2  # Original system dimension
    dim_lifted = 4  # Lifted system dimension
    batch_size = 2**15  # Batch size
    epochs = 4000 # Epcohs
    steps_til_summary = 200  # Printing summary after 200 epcohs
    training_mode = False  # Training from scratch
    loading_mode = True  # Loading trained model
    nums_seqs = 1000  # Number of training seqs
    test_seqs = 100  # Number of testing seqs
    tf = 10  # Time during for seqs
    ts = 5e-2  # Time steps


def potential(x1, x2):
    """Defining potential of pendulum."""
    return (1 / 2) * x2**2 - np.cos(x1)


class Pendulum(Dataset):
    """Preparing dataloader for Pendulum Example."""

    def __init__(self, Xs, deri_Xs):
        super().__init__()

        self.Xs = torch.tensor(Xs.reshape(-1, Xs.shape[-1])).float().to(device)
        self.deri_Xs = (
            torch.tensor(deri_Xs.reshape(-1, deri_Xs.shape[-1])).float().to(device)
        )

    def __len__(self):
        return len(self.Xs)

    def __getitem__(self, idx):
        return {"coords": self.Xs[idx]}, {"deri_info": self.deri_Xs[idx]}


if __name__ == "__main__":
    OPT = ParametersPendulum()

    ROOT_PATH = "../results/Pendulum/"
    PATH_MODELS = ROOT_PATH + "./pendulum_models.pt"
    utils.cond_mkdir(ROOT_PATH)  # Create folder if does not exist.

    utils.save_parameters_configs(
        ParametersPendulum, ROOT_PATH + "pendulum_experiment_data.txt"
    )

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    #######################
    # Data Generation ####
    #######################

    dyn_model = dynamical_models.nonlinear_pendulum
    tf = OPT.tf
    ts = OPT.ts
    num_seqs = OPT.nums_seqs

    RANGE_INITIALCONDS = (-2, 2)
    #
    ts = np.arange(0, tf, ts)

    Xs = np.zeros((num_seqs, len(ts), OPT.dim_x))
    deri_Xs = np.zeros((num_seqs, len(ts), OPT.dim_x))

    X_inits = np.zeros((num_seqs, OPT.dim_x))

    MAX_POTENTIAL = 0.75

    i = 0
    while i < num_seqs:

        # defining initial condition
        x0 = np.random.uniform(RANGE_INITIALCONDS[0], RANGE_INITIALCONDS[1], 2)

        if potential(x0[0], x0[1]) <= MAX_POTENTIAL:  # Checking for potential
            X_inits[i] = x0
            # Solving initial value problem
            sol = solve_ivp(
                lambda t, x: dyn_model(x, t), [ts[0], ts[-1]], x0, t_eval=ts
            )
            Xs[i] = np.transpose(sol.y)

            # Compute derivates
            for j in range(len(Xs[i])):
                deri_Xs[i, j, :] = dyn_model(Xs[i, j, :], t=0)

            i += 1

    dataset = Pendulum(Xs, deri_Xs)
    dataloader = DataLoader(dataset, shuffle=True, batch_size=OPT.batch_size)

    ########################
    # Defining Modesl #####
    ########################
    models = {}
    models["encoder"] = modules.SingleBVPNet(
        in_features=OPT.dim_x,
        out_features=OPT.dim_lifted,
        type=OPT.model,
        mode=OPT.mode,
        hidden_features=OPT.hidden_features,
        num_hidden_layers=OPT.num_hidden_layers,
        print_mode=True,
    ).to(device)

    models["decoder"] = modules.LinearModel(
        in_features=OPT.dim_lifted,
        out_features=OPT.dim_x,
        print_mode=True,
        zero_init=True,
    ).to(device)

    models["quadModel"] = modules.QuadModel(
        in_features=OPT.dim_lifted,
        out_features=OPT.dim_lifted,
        print_mode=False,
        zero_init=True,
    ).to(device)


    # Define optimizer
    optim = torch.optim.Adam(
        [
            {
                "params": models["encoder"].parameters(),
                "lr": 1e-3,
                "weight_decay": 1e-4,
            },
            {
                "params": models["decoder"].parameters(),
                "lr": 5e-3,
                "weight_decay": 1e-5,
            },
            {
                "params": models["quadModel"].parameters(),
                "lr": 5e-3,
                "weight_decay": 1e-5,
            },
        ]
    )

    if OPT.training_mode:
        models, losses = train_quadembs_pendulum(
            models,
            dataloader,
            OPT.epochs,
            OPT.steps_til_summary,
            optim,
            epochs_decaylr=1000,
        )

        torch.save(
            {
                "encoder": models["encoder"].state_dict(),
                "decoder": models["decoder"].state_dict(),
                "quadModel": models["quadModel"].state_dict(),
            },
            PATH_MODELS,
        )

        fig, ax = utils.plot_dictionary(losses)
        fig.savefig(
            ROOT_PATH + "pendulum_training.png", bbox_inches="tight", pad_inches=0
        )

        TOL_PRUNE = 5e-2
        ## Doing pruning of the quadratic model

        Ws = models["quadModel"].fc.weight.detach().clone()
        Mask_Ws = (Ws.abs() > TOL_PRUNE).type(torch.float)
        models["quadModel"].fc.weight = torch.nn.Parameter(Ws * Mask_Ws)
        models["quadModel"].fc.weight.register_hook(lambda grad: grad.mul_(Mask_Ws))

        # Define optimizer
        print(models["quadModel"].fc.weight)
        print(models["decoder"].fc.weight)
        optim = torch.optim.Adam(
            [
                {
                    "params": models["encoder"].parameters(),
                    "lr": 1e-3 / 2,
                    "weight_decay": 1e-4,
                },
                {
                    "params": models["decoder"].parameters(),
                    "lr": 5e-3 / 2,
                    "weight_decay": 1e-5,
                },
                {
                    "params": models["quadModel"].parameters(),
                    "lr": 5e-3 / 2,
                    "weight_decay": 1e-5,
                },
            ]
        )

        models, losses = train_quadembs_pendulum(
            models,
            dataloader,
            int(OPT.epochs // 2),
            OPT.steps_til_summary,
            optim,
            epochs_decaylr=500,
        )
        torch.save(
            {
                "encoder": models["encoder"].state_dict(),
                "decoder": models["decoder"].state_dict(),
                "quadModel": models["quadModel"].state_dict(),
            },
            PATH_MODELS,
        )

        fig, ax = utils.plot_dictionary(losses)
        #      plt.show()
        fig.savefig(
            ROOT_PATH + "pendulum_training.png", bbox_inches="tight", pad_inches=0
        )

    if OPT.loading_mode:
        if device.type == "cpu":
            checkpoint = torch.load('./Trained_models/pendulum_models.pt', map_location="cpu")
        else:
            checkpoint = torch.load('./Trained_models/pendulum_models.pt')

        models["encoder"].load_state_dict(checkpoint["encoder"])
        models["decoder"].load_state_dict(checkpoint["decoder"])
        models["quadModel"].load_state_dict(checkpoint["quadModel"])

    #####################################
    ######## TESTING and Plotting #######
    #####################################

    A = (
        models["quadModel"]
        .fc.weight[:, : OPT.dim_lifted]
        .detach()
        .clone()
        .cpu()
        .numpy()
    )
    H = (
        models["quadModel"]
        .fc.weight[:, OPT.dim_lifted :]
        .detach()
        .clone()
        .cpu()
        .numpy()
    )
    B = models["quadModel"].fc.bias.detach().clone().cpu().numpy().flatten()
    print(np.linalg.eig(A)[0])
    total_err = 0
    tracking_test_err = []
    k = 0
    ts = np.arange(0, 30, OPT.ts)

    while k < OPT.test_seqs:
        i = 0
        testXs = np.zeros((1, len(ts), OPT.dim_x))
        deri_testXs = np.zeros((1, len(ts), OPT.dim_x))

        testX_inits = np.zeros((1, OPT.dim_x))

        # defining initial condition
        x0 = np.random.uniform(RANGE_INITIALCONDS[0], RANGE_INITIALCONDS[1], 2)

        if potential(x0[0], x0[1]) <= MAX_POTENTIAL:  # Checking for potential
            # Solving initial value problem
            sol = solve_ivp(
                lambda t, x: dyn_model(x, t), [ts[0], ts[-1]], x0, t_eval=ts
            )
            testXs[i] = np.transpose(sol.y)

            # Compute derivates
            for j in range(len(Xs[i])):
                deri_testXs[i, j, :] = dyn_model(Xs[i, j, :], t=0)
            k += 1
        else:
            continue

        testdataset = Pendulum(testXs, deri_testXs)
        testdataloader = DataLoader(
            testdataset, shuffle=False, batch_size=OPT.batch_size
        )

        with torch.no_grad():
            test_x, test_dx = next(iter(testdataloader))
            enc_output = models["encoder"](test_x)
            x0 = enc_output["model_out"][0].detach().clone().cpu().numpy()

            sol = solve_ivp(
                lambda t, x: utils.quad_model(x, t, A, H, B),
                [ts[0], ts[-1]],
                x0,
                t_eval=ts.reshape(
                    -1,
                ),
            )

        quad_int = torch.from_numpy(np.transpose(sol.y)).float()

        dec_output = (
            models["decoder"]
            .to(device)(quad_int.to(device))["model_out"]
            .detach()
            .clone()
            .cpu()
            .numpy()
        )

        norm_orig = np.sqrt(
            ((test_x["coords"].detach().clone().cpu().numpy()) ** 2).sum()
        )
        err = (
            (dec_output - test_x["coords"].detach().clone().cpu().numpy()) ** 2
        ).mean()

        total_err += err

        tracking_test_err.append(err)
        print(
            f"Error for {k}th test is {err:.2e}, and the mean error {total_err/k:.2e}"
        )

        fig = plt.figure(figsize=(6.5, 5))
        ax = fig.add_subplot(111)

        ax.plot(quad_int)
        ax.plot(enc_output["model_out"].detach().clone().cpu().numpy(), "--")
        ax.set(xlabel="Time", ylabel="$\{z_1(t), z_2(t), z_2(t), z_4(t)\}$")

        fig.savefig(
            ROOT_PATH + f"pendulum_embeddings_testing_{i}.png",
            bbox_inches="tight",
            pad_inches=0,
        )
        plt.close(fig)

        fig = plt.figure(figsize=(14, 5))
        ax = fig.add_subplot(122)

        ax.plot(
            ts,
            test_x["coords"].detach().cpu().numpy()[..., 0],
            "bo",
            markersize=4,
            label="Truth",
        )
        ax.plot(ts, test_x["coords"].detach().cpu().numpy()[..., 1], "bo", markersize=4)

        ax.plot(
            ts,
            dec_output[..., 0],
            linewidth=2.5,
            color="m",
            label="Estimated via Quad-Emb.",
        )
        ax.plot(ts, dec_output[..., 1], linewidth=2.5, color="m")
        ax.set(xlabel="Time", ylabel="$\{x_1(t), x_2(t)\}$")
        ax.legend(loc=1)

        ax = fig.add_subplot(121)
        ax.plot(
            test_x["coords"].detach().cpu().numpy()[..., 0],
            test_x["coords"].detach().cpu().numpy()[..., 1],
            "bo",
            markersize=4,
            label="Truth",
        )
        ax.plot(
            dec_output[..., 0],
            dec_output[..., 1],
            linewidth=2.5,
            color="m",
            label="Estimated via Quad-Emb.",
        )
        ax.set(xlabel="$x_1(t)$", ylabel="$x_2(t)$")
        ax.legend(loc=1)

        fig.savefig(
            ROOT_PATH + f"pendulum_testing_{k}.png",
            bbox_inches="tight",
            pad_inches=0,
            dpi=300,
        )
        fig.savefig(
            ROOT_PATH + f"pendulum_testing_lowres_{k}.png",
            bbox_inches="tight",
            pad_inches=0,
            dpi=75,
        )

        plt.close(fig)

    for i in [10, 15, 20, 25]:
        utils.plot_loghist(tracking_test_err, i)
        plt.savefig(
            ROOT_PATH + f"histogram_{i}_plot.png", bbox_inches="tight", pad_inches=0
        )
        plt.close()

    # Plotting learned embeddings
    plot_dataloader = DataLoader(dataset, shuffle=True, batch_size=len(dataset))
    with torch.no_grad():
        x_, _ = next(iter(plot_dataloader))
        enc_output = models["encoder"](x_)
        z = enc_output["model_out"].cpu().numpy()
        x = x_["coords"].cpu().numpy()

    fig = plt.figure(figsize=(20, 12))
    ax = fig.add_subplot(141, projection="3d")
    ax.scatter(x[:, 0], x[:, 1], z[:, 0], c="b", marker="o", s=0.01)
    ax.set(xlabel="$x_1$", ylabel="$x_2$", zlabel="$z_1$")

    ax = fig.add_subplot(142, projection="3d")
    ax.scatter(x[:, 0], x[:, 1], z[:, 1], c="m", marker="o", s=0.01)
    ax.set(xlabel="$x_1$", ylabel="$x_2$", zlabel="$z_2$")

    ax = fig.add_subplot(143, projection="3d")
    ax.scatter(x[:, 0], x[:, 1], z[:, 2], c="tab:orange", marker="o", s=0.01)
    ax.set(xlabel="$x_1$", ylabel="$x_2$", zlabel="$z_3$")

    ax = fig.add_subplot(144, projection="3d")
    ax.scatter(x[:, 0], x[:, 1], z[:, 3], c="tab:cyan", marker="o", s=0.01)
    ax.set(xlabel="$x_1$", ylabel="$x_2$", zlabel="$z_4$")

    # plt.show()
    fig.savefig(
        ROOT_PATH + "pendulum_quad_embeddings.png", bbox_inches="tight", pad_inches=0
    )
