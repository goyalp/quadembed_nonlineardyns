#!/usr/bin/env python
# coding: utf-8

import os
import sys
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
import torch
from scipy.integrate import solve_ivp
from torch.utils.data import DataLoader, Dataset

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ["KMP_DUPLICATE_LIB_OK"] = "True"

import modules
import utils.utils as utils
import utils.dynamical_models as dynamical_models
from functions.training import train_quadembs_mm


SIZE = 20
params = {
    "legend.fontsize": "large",
    "figure.figsize": (20, 8),
    "axes.labelsize": SIZE,
    "axes.titlesize": SIZE,
    "xtick.labelsize": SIZE * 0.75,
    "ytick.labelsize": SIZE * 0.75,
    "axes.titlepad": 25,
}
plt.rcParams.update(params)


SEED = 42
utils.reproducibility_seed(SEED)


@dataclass
class ParametersMM:
    """Defining importart parameters."""

    model = "selu"  # Activation function
    mode = "mlp"  # Muti-layer perceptron
    hidden_features = 8  # Number of neurons
    num_hidden_layers = 3  # Number of layers
    dim_x = 1  # Original system dimension
    dim_lifted = 2  # Lifted system dimension
    batch_size = 2**15  # Batch size
    epochs = 2500  # Epcohs
    steps_til_summary = 200  # Printing summary after 200 epcohs
    training_mode = False  # Training from scratch
    loading_mode = True  # Loading trained model
    nums_seqs = 50  # Number of training seqs
    test_seqs = 100  # Number of testing seqs
    tf = 15  # Time during for seqs
    ts = 1e-2  # Time steps


class MM(Dataset):
    """Preparing dataloader for Miches-Menten Example."""

    def __init__(self, Xs, deri_Xs):
        super().__init__()

        self.Xs = torch.tensor(Xs.reshape(-1, Xs.shape[-1])).float().to(device)
        self.deri_Xs = (
            torch.tensor(deri_Xs.reshape(-1, deri_Xs.shape[-1])).float().to(device)
        )

    def __len__(self):
        return len(self.Xs)

    def __getitem__(self, idx):
        return {"coords": self.Xs[idx]}, {"deri_info": self.deri_Xs[idx]}


if __name__ == "__main__":

    OPT = ParametersMM()

    ROOT_PATH = "../results/MM/"
    PATH_MODELS = ROOT_PATH + "MM_models.pt"
    utils.cond_mkdir(ROOT_PATH)

    utils.save_parameters_configs(ParametersMM, ROOT_PATH + "MM_experiment_data.txt")

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    dyn_model = dynamical_models.mm_kinetics

    #######################
    # Data Generation ####
    #######################

    tf = OPT.tf
    ts = OPT.ts
    num_seqs = OPT.nums_seqs

    RANGE_INITIALCONDS = (1, 3)
    #
    ts = np.arange(0, tf, ts)

    Xs = np.zeros((num_seqs, len(ts), OPT.dim_x))
    deri_Xs = np.zeros((num_seqs, len(ts), OPT.dim_x))

    X_inits = np.zeros((num_seqs, OPT.dim_x))

    i = 0
    while i < num_seqs:
        # defining initial condition
        x0 = RANGE_INITIALCONDS[0] + np.random.rand(
            1,
        ) * (RANGE_INITIALCONDS[1] - RANGE_INITIALCONDS[0])
        X_inits[i] = x0

        # Solving initial value problem
        sol = solve_ivp(lambda t, x: dyn_model(x, t), [ts[0], ts[-1]], x0, t_eval=ts)
        Xs[i] = np.transpose(sol.y)

        # Compute derivates
        for j in range(len(Xs[i])):
            deri_Xs[i, j, :] = dyn_model(Xs[i, j, :], t=0)

        i += 1

    dataset = MM(Xs - 2.0, deri_Xs)
    dataloader = DataLoader(dataset, shuffle=True, batch_size=OPT.batch_size)

    ########################
    # Defining Modesl #####
    ########################

    models = {}
    models["encoder"] = modules.SingleBVPNet(
        in_features=OPT.dim_x,
        out_features=OPT.dim_lifted,
        type=OPT.model,
        mode=OPT.mode,
        hidden_features=OPT.hidden_features,
        num_hidden_layers=OPT.num_hidden_layers,
        print_mode=True,
    ).to(device)

    models["decoder"] = modules.LinearModel(
        in_features=OPT.dim_lifted,
        out_features=OPT.dim_x,
        print_mode=True,
        zero_init=True,
    ).to(device)

    models["quadModel"] = modules.QuadModel(
        in_features=OPT.dim_lifted,
        out_features=OPT.dim_lifted,
        print_mode=False,
        zero_init=True,
    ).to(device)

    # Define OPTimizer
    optim = torch.optim.Adam(
        [
            {
                "params": models["encoder"].parameters(),
                "lr": 1e-3,
                "weight_decay": 1e-4,
            },
            {
                "params": models["decoder"].parameters(),
                "lr": 5e-3,
                "weight_decay": 1e-4,
            },
            {
                "params": models["quadModel"].parameters(),
                "lr": 5e-3,
                "weight_decay": 1e-4,
            },
        ]
    )

    if OPT.training_mode:
        models, losses = train_quadembs_mm(
            models,
            dataloader,
            OPT.epochs,
            OPT.steps_til_summary,
            optim,
            epochs_decaylr=1000,
        )

        torch.save(
            {
                "encoder": models["encoder"].state_dict(),
                "decoder": models["decoder"].state_dict(),
                "quadModel": models["quadModel"].state_dict(),
            },
            PATH_MODELS,
        )

        fig, ax = utils.plot_dictionary(losses)
        # plt.show()
        fig.savefig(ROOT_PATH + "MM_training.png", bbox_inches="tight", pad_inches=0)

        TOL_PRUNE = 1e-2

        Ws = models["quadModel"].fc.weight.detach().clone()
        Mask_Ws = (Ws.abs() > TOL_PRUNE).type(torch.float)
        models["quadModel"].fc.weight = torch.nn.Parameter(Ws * Mask_Ws)
        models["quadModel"].fc.weight.register_hook(lambda grad: grad.mul_(Mask_Ws))

        # Define optimizer
        print(models["quadModel"].fc.weight)
        print(models["decoder"].fc.weight)
        optim = torch.optim.Adam(
            [
                {
                    "params": models["encoder"].parameters(),
                    "lr": 1e-3 / 2,
                    "weight_decay": 1e-4,
                },
                {
                    "params": models["decoder"].parameters(),
                    "lr": 5e-3 / 2,
                    "weight_decay": 1e-4,
                },
                {
                    "params": models["quadModel"].parameters(),
                    "lr": 5e-3 / 2,
                    "weight_decay": 1e-4,
                },
            ]
        )

        models, losses = train_quadembs_mm(
            models,
            dataloader,
            int(OPT.epochs // 2),
            OPT.steps_til_summary,
            optim,
            epochs_decaylr=500,
        )
        torch.save(
            {
                "encoder": models["encoder"].state_dict(),
                "decoder": models["decoder"].state_dict(),
                "quadModel": models["quadModel"].state_dict(),
            },
            PATH_MODELS,
        )

        fig, ax = utils.plot_dictionary(losses)
        fig.savefig(
            ROOT_PATH + "MM_training_prune.png", bbox_inches="tight", pad_inches=0
        )

    if OPT.loading_mode:
        print("loading models....")
        if device.type == "cpu":
            checkpoint = torch.load('./Trained_models/MM_models.pt', map_location="cpu")
        else:
            checkpoint = torch.load('./Trained_models/MM_models.pt')

        models["encoder"].load_state_dict(checkpoint["encoder"])
        models["decoder"].load_state_dict(checkpoint["decoder"])
        models["quadModel"].load_state_dict(checkpoint["quadModel"])

    ##############################
    # Testing models on new data
    ##############################
    A = (
        models["quadModel"]
        .fc.weight[:, : OPT.dim_lifted]
        .detach()
        .clone()
        .cpu()
        .numpy()
    )
    H = (
        models["quadModel"]
        .fc.weight[:, OPT.dim_lifted:]
        .detach()
        .clone()
        .cpu()
        .numpy()
    )
    B = models["quadModel"].fc.bias.detach().clone().cpu().numpy().flatten()

    total_err = 0
    tracking_test_err = []
    k = 0
    while k < OPT.test_seqs:
        testXs = np.zeros((1, len(ts), OPT.dim_x))
        deri_testXs = np.zeros((1, len(ts), OPT.dim_x))
        testX_inits = np.zeros((1, OPT.dim_x))

        # defining initial condition
        x0 = RANGE_INITIALCONDS[0] + np.random.rand(
            1,
        ) * (RANGE_INITIALCONDS[1] - RANGE_INITIALCONDS[0])

        # Solving initial value problem
        sol = solve_ivp(lambda t, x: dyn_model(x, t), [ts[0], ts[-1]], x0, t_eval=ts)
        testXs[0] = np.transpose(sol.y)

        # Compute derivates
        for j in range(len(Xs[0])):
            deri_testXs[0, j, :] = dyn_model(Xs[0, j, :], t=0)

        k += 1

        testdataset = MM(testXs - 2.0, deri_testXs)
        testdataloader = DataLoader(
            testdataset, shuffle=False, batch_size=OPT.batch_size
        )

        with torch.no_grad():
            test_x, test_dx = next(iter(testdataloader))
            enc_output = models["encoder"](test_x)

            x0 = enc_output["model_out"][0].detach().clone().cpu().numpy()
            sol = solve_ivp(
                lambda t, x: utils.quad_model(x, t, A, H, B),
                [ts[0], ts[-1]],
                x0,
                t_eval=ts.reshape(
                    -1,
                ),
            )

        quad_int = torch.from_numpy(np.transpose(sol.y)).float()

        dec_output = (
            models["decoder"]
            .to(device)(quad_int.to(device))["model_out"]
            .detach()
            .clone()
            .cpu()
            .numpy()
        )

        if len(dec_output) != len(test_x["coords"]):
            unstable_modes.append(x0)
            print("found unstable mode")
            continue

        err = (
            (dec_output - test_x["coords"].detach().clone().cpu().numpy()) ** 2
        ).mean()
        total_err += err
        tracking_test_err.append(err)
        print(
            f"Error for {k}th test is {err:.2e}, and the mean error {total_err/k:.2e}"
        )

        fig = plt.figure(figsize=(6, 4))

        ax = fig.add_subplot(111)
        ax.plot(ts,
            test_x["coords"].detach().cpu().numpy()[..., 0] + 2.0,
            "bo",
            markersize=4,
            label="Truth",
        )
        ax.plot(ts,
            dec_output[..., 0] + 2.0,
            linewidth=2.5,
            color="m",
            label="Estimated via Quad-Emb.",
        )
        ax.set(xlabel="$Time$", ylabel="$x(t)$")
        ax.legend(loc=1)

        fig.savefig(
            ROOT_PATH + f"MM_testing_{k}.png",
            bbox_inches="tight",
            pad_inches=0,
            dpi=300,
        )
        plt.close(fig)

    utils.plot_loghist(tracking_test_err, 25)
    plt.savefig(ROOT_PATH + "histogram_plot.png", bbox_inches="tight", pad_inches=0)

    ####################################
    # Plotting the learned embeddings
    ####################################
    plot_dataloader = DataLoader(dataset, shuffle=True, batch_size=len(dataset))
    with torch.no_grad():
        x_, _ = next(iter(plot_dataloader))
        enc_output = models["encoder"](x_)
        z = enc_output["model_out"].cpu().numpy()
        x = x_["coords"].cpu().numpy()

    fig = plt.figure(figsize=(13, 5))
    ax = fig.add_subplot(141)
    ax.scatter(x[:, 0], z[:, 0], c="b", marker="o", s=0.01)
    ax.set(xlabel="$x_1$", ylabel="$z_1$")

    ax = fig.add_subplot(142)
    ax.scatter(x[:, 0], z[:, 1], c="m", marker="o", s=0.01)
    ax.set(xlabel="$x_1$", ylabel="$z_2$")
    fig.savefig(ROOT_PATH + "MM_quad_embeddings.png", bbox_inches="tight", pad_inches=0)
