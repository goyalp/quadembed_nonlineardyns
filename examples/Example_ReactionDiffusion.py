#!/usr/bin/env python
# coding: utf-8

import os
import sys
from dataclasses import dataclass

import matplotlib.pyplot as plt
# use data coordinates for the x-axis and the axes coordinates for the y-axis
import matplotlib.transforms as mtransforms
import numpy as np
import scipy
import scipy.io
import torch
from scipy.integrate import solve_ivp
from torch.utils.data import DataLoader, Dataset

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ["KMP_DUPLICATE_LIB_OK"] = "True"

import modules
import utils.utils as utils
from functions.training import train_quademds_RD

# Plotting parameter setting
SIZE = 20
params = {
    "legend.fontsize": "large",
    "figure.figsize": (20, 8),
    "axes.labelsize": SIZE,
    "axes.titlesize": SIZE,
    "xtick.labelsize": SIZE * 0.75,
    "ytick.labelsize": SIZE * 0.75,
    "axes.titlepad": 25,
}
plt.rcParams.update(params)

# For reproducbility
SEED = 42
utils.reproducibility_seed(SEED)


@dataclass
class ParametersRDModel:
    """Parameters for reaction diffusion model."""

    model = "selu"
    mode = "mlp"
    hidden_features = 16
    num_hidden_layers = 2
    dim_x = 2
    dim_lifted = 2
    batch_size = 100
    epochs = 2500
    steps_til_summary = 200
    training_mode = False
    loading_mode = True


class RDModelDataset_Reduced(Dataset):
    """Preparing dataloader for reaction diffusion model."""

    def __init__(self, indices):
        super().__init__()
        dataRDModel_reduced = scipy.io.loadmat("../data/reaction_diffusion_reduced.mat")

        uv = torch.tensor(dataRDModel_reduced["UVr"][..., :indices]).permute(1, 0)

        duv = torch.tensor(dataRDModel_reduced["dUVr"][..., :indices]).permute(1, 0)

        self.Xs = uv.float().to(device)
        self.deri_Xs = duv.float().to(device)

    def __len__(self):
        return len(self.Xs)

    def __getitem__(self, idx):
        return {"coords": self.Xs[idx]}, {"deri_info": self.deri_Xs[idx]}


if __name__ == "__main__":

    opt = ParametersRDModel()
    ROOT_PATH = "../results/RDModel_reduced/"
    PATH_MODELS = ROOT_PATH + "./RD_reduced_models.pt"
    utils.cond_mkdir(ROOT_PATH)

    utils.save_parameters_configs(
        ParametersRDModel, ROOT_PATH + "RDModel_experiment_data.txt"
    )

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    #######################
    # Data Generation ####
    #######################

    dataRDModel = scipy.io.loadmat("../data/reaction_diffusion.mat")
    x_pts = len(dataRDModel["x"][0])
    y_pts = len(dataRDModel["y"][0])
    U = dataRDModel["uf"].reshape(x_pts * y_pts, -1)
    V = dataRDModel["vf"].reshape(x_pts * y_pts, -1)
    UV = np.concatenate([U, V])

    dU = dataRDModel["duf"].reshape(x_pts * y_pts, -1)
    dV = dataRDModel["dvf"].reshape(x_pts * y_pts, -1)
    dUV = np.concatenate([dU, dV])

    X, S, Y = np.linalg.svd(np.concatenate([UV, dUV], axis=1))

    r = opt.dim_x  # Setting order of reduced-order models

    Xr = X[:, :r]
    Sr = S[:r]
    Yr = Y[:r, :]
    recUV = Xr @ np.diag(Sr) @ Yr[:, :99]
    recdUV = (Xr @ np.diag(Sr) @ Yr[:, 99:]).T @ dUV

    UVr = ((Xr.T) @ UV) / (S[0] ** 0.5)
    dUVr = ((Xr.T) @ dUV) / (S[0] ** 0.5)

    reduced_data = {"r": r, "UVr": UVr, "dUVr": dUVr, "X": X, "S": S, "Y": Y}
    scipy.io.savemat(
        "../data/reaction_diffusion_reduced.mat", reduced_data
    )  # Saving for future reference.

    dataRDModel = scipy.io.loadmat("../data/reaction_diffusion_reduced.mat")
    r = dataRDModel["r"]

    dataset = RDModelDataset_Reduced(75)  # Considering first 75 points for training
    dataloader = DataLoader(dataset, shuffle=False, batch_size=opt.batch_size)

    ########################
    # Defining Modesl #####
    ########################
    models = {}
    models["encoder"] = modules.SingleBVPNet(
        in_features=opt.dim_x,
        out_features=opt.dim_lifted,
        type=opt.model,
        mode=opt.mode,
        hidden_features=opt.hidden_features,
        num_hidden_layers=opt.num_hidden_layers,
        print_mode=True,
    ).to(device)

    models["decoder"] = modules.LinearModel(
        in_features=opt.dim_lifted,
        out_features=opt.dim_x,
        print_mode=True,
        zero_init=True,
    ).to(device)

    models["quadModel"] = modules.QuadModel(
        in_features=opt.dim_lifted,
        out_features=opt.dim_lifted,
        print_mode=True,
        zero_init=True,
    ).to(device)

    # Data related features
    INFO = {"dim_x": opt.dim_x, "dim_lifted": opt.dim_lifted}

    # Define optimizer
    optim = torch.optim.Adam(
        [
            {
                "params": models["encoder"].parameters(),
                "lr": 1e-3,
                "weight_decay": 1e-4,
            },
            {
                "params": models["decoder"].parameters(),
                "lr": 5e-3,
                "weight_decay": 1e-4,
            },
            {
                "params": models["quadModel"].parameters(),
                "lr": 5e-3,
                "weight_decay": 1e-4,
            },
        ]
    )

    if opt.training_mode:
        models, losses = train_quademds_RD(
            models,
            dataloader,
            opt.epochs,
            opt.steps_til_summary,
            optim,
            epochs_decaylr=1000,
        )

        torch.save(
            {
                "encoder": models["encoder"].state_dict(),
                "decoder": models["decoder"].state_dict(),
                "quadModel": models["quadModel"].state_dict(),
            },
            PATH_MODELS,
        )

        fig, ax = utils.plot_dictionary(losses)
        fig.savefig(
            ROOT_PATH + "RDModel_training.png", bbox_inches="tight", pad_inches=0
        )

        TOL_PRUNE = 5e-2

        ## Doing pruning of the quadratic model
        Ws = models["quadModel"].fc.weight.detach().clone()
        Mask_Ws = (Ws.abs() > TOL_PRUNE).type(torch.float)
        models["quadModel"].fc.weight = torch.nn.Parameter(Ws * Mask_Ws)
        models["quadModel"].fc.weight.register_hook(lambda grad: grad.mul_(Mask_Ws))

        # Define optimizer
        print(models["quadModel"].fc.weight)
        print(models["decoder"].fc.weight)
        optim = torch.optim.Adam(
            [
                {
                    "params": models["encoder"].parameters(),
                    "lr": 1e-3 / 2,
                    "weight_decay": 1e-4,
                },
                {
                    "params": models["decoder"].parameters(),
                    "lr": 5e-3 / 2,
                    "weight_decay": 1e-4,
                },
                {
                    "params": models["quadModel"].parameters(),
                    "lr": 5e-3 / 2,
                    "weight_decay": 1e-4,
                },
            ]
        )

        models, losses = train_quademds_RD(
            models,
            dataloader,
            int(opt.epochs // 2),
            opt.steps_til_summary,
            optim,
            epochs_decaylr=500,
        )
        torch.save(
            {
                "encoder": models["encoder"].state_dict(),
                "decoder": models["decoder"].state_dict(),
                "quadModel": models["quadModel"].state_dict(),
            },
            PATH_MODELS,
        )

        fig, ax = utils.plot_dictionary(losses)
        fig.savefig(
            ROOT_PATH + "RDModel_training_prune.png", bbox_inches="tight", pad_inches=0
        )

    if opt.loading_mode:
        if device.type == "cpu":
            checkpoint = torch.load('./Trained_models/RD_reduced_models.pt', map_location="cpu")
        else:
            checkpoint = torch.load('./Trained_models/RD_reduced_models.pt')

        models["encoder"].load_state_dict(checkpoint["encoder"])
        models["decoder"].load_state_dict(checkpoint["decoder"])
        models["quadModel"].load_state_dict(checkpoint["quadModel"])

    #####################################
    ######## TESTING and Plotting #######
    #####################################

    A = (
        models["quadModel"]
        .fc.weight[:, : opt.dim_lifted]
        .detach()
        .clone()
        .cpu()
        .numpy()
    )
    H = (
        models["quadModel"]
        .fc.weight[:, opt.dim_lifted :]
        .detach()
        .clone()
        .cpu()
        .numpy()
    )
    B = (
        models["quadModel"]
        .fc.bias.reshape(
            -1,
        )
        .detach()
        .clone()
        .cpu()
        .numpy()
    )

    def quad_model(x, t, A, H, B):
        """Defining a quadratic model right-hand side."""
        return A @ x + H @ (np.kron(x, x)) + B

    dataRDModel_reduced = scipy.io.loadmat("../data/reaction_diffusion_reduced.mat")
    uv = dataRDModel_reduced["UVr"]

    dataRDModel = scipy.io.loadmat("../data/reaction_diffusion.mat")
    ts = dataRDModel["t"] - 0.1
    ts = np.arange(0, 25, 0.05)

    with torch.no_grad():
        x, _ = next(iter(dataloader))
        enc_output = models["encoder"](x)
        x0 = enc_output["model_out"][0].detach().clone().cpu().numpy()

        sol = solve_ivp(
            lambda t, x: quad_model(x, t, A, H, B),
            [ts[0], ts[-1]],
            x0,
            t_eval=ts.reshape(
                -1,
            ),
        )

        quad_int = torch.from_numpy(np.transpose(sol.y)).float()
        dec_output = (
            models["decoder"]
            .to(device)(quad_int.to(device))["model_out"]
            .detach()
            .clone()
            .cpu()
            .numpy()
        )

    fig = plt.figure(figsize=(9, 4))
    ax = fig.add_subplot(111)
    ax.plot(ts, dec_output[..., 0], "--", color="b", label="Quad-embed model")
    ax.plot(ts, dec_output[..., 1], "--", color="b")

    ax.plot(
        ts[:99],
        dataRDModel_reduced["UVr"].T[..., 0],
        linestyle="None",
        marker="o",
        markersize=5,
        color="m",
        markevery=4,
        label="Truth",
    )
    ax.plot(
        ts[:99],
        dataRDModel_reduced["UVr"].T[..., 1],
        linestyle="None",
        marker="o",
        markersize=5,
        color="m",
        markevery=4,
    )
    ax.set(xlabel="Time", ylabel="POD Coeffs")

    trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)

    ax.fill_between(
        ts[:75],
        0,
        1,
        facecolor="green",
        alpha=0.20,
        transform=trans,
        label="Training domain",
    )
    ax.fill_between(
        ts[75:99],
        0,
        1,
        facecolor="blue",
        alpha=0.20,
        transform=trans,
        label="Testing domain",
    )
    ax.fill_between(
        ts[99:],
        0,
        1,
        facecolor="red",
        alpha=0.20,
        transform=trans,
        label="Extrapolation domain",
    )
    ax.legend(
        loc="upper right",
        bbox_to_anchor=(1.4, 1.0),
        ncol=1,
        fancybox=True,
        shadow=True,
        fontsize=16,
    )
    fig.savefig(
        ROOT_PATH + "RD_model_POD_Coeffs.png",
        bbox_inches="tight",
        pad_inches=0,
        dpi=300,
    )
    plt.close(fig)

    full_state_recon = (Xr @ dec_output.T) * (S[0] ** 0.5)
    X_grid, Y_grid = np.meshgrid(dataRDModel["x"], dataRDModel["y"])

    ###### Training regime ######
    for idx in range(75):

        fig = plt.figure(figsize=(14, 4.5))
        ax = fig.add_subplot(121)
        img = ax.contour(
            X_grid, Y_grid, UV[: x_pts * y_pts, idx].reshape(x_pts, y_pts), 20
        )
        ax.set(xlabel="x", ylabel="y", title=f"Ground truth (time: {idx*0.05:.2f}s)")
        plt.colorbar(img, ax=ax)

        ax = fig.add_subplot(122)
        img = ax.contour(
            X_grid,
            Y_grid,
            full_state_recon[: x_pts * y_pts, idx].reshape(x_pts, y_pts),
            20,
        )
        ax.set(xlabel="x", ylabel="y", title=f"Quad emb. (time: {idx*0.05:.2f}s)")
        plt.colorbar(img, ax=ax)

        fig.savefig(
            ROOT_PATH + f"RD_model_training_{idx:04d}.png",
            bbox_inches="tight",
            pad_inches=0,
            dpi=300,
        )
        plt.close(fig)

    ############################
    # Testing regime ###########
    ############################
    for idx in range(75, 99):

        fig = plt.figure(figsize=(14, 4.5))
        ax = fig.add_subplot(121)
        img = ax.contour(
            X_grid, Y_grid, UV[: x_pts * y_pts, idx].reshape(x_pts, y_pts), 20
        )
        ax.set(xlabel="x", ylabel="y", title=f"Ground truth (time: {idx*0.05:.2f}s)")
        plt.colorbar(img, ax=ax)

        ax = fig.add_subplot(122)
        img = ax.contour(
            X_grid,
            Y_grid,
            full_state_recon[: x_pts * y_pts, idx].reshape(x_pts, y_pts),
            20,
        )
        ax.set(xlabel="x", ylabel="y", title=f"Quad emb. (time: {idx*0.05:.2f}s)")
        plt.colorbar(img, ax=ax)

        fig.savefig(
            ROOT_PATH + f"RD_model_testing_{idx:04d}.png",
            bbox_inches="tight",
            pad_inches=0,
            dpi=300,
        )
        plt.close(fig)

    # ###### Extrapolation_1 ######
    # for idx in range(99,1000):

    #     fig = plt.figure(figsize=(11,4.5))
    # #     ax = fig.add_subplot(121)
    # #     img = ax.contour(X_grid, Y_grid, UV[:x_pts*y_pts, idx].reshape(x_pts, y_pts), 20)
    # #     ax.set(xlabel = 'x', ylabel = 'y', title = f'Ground truth at time: {idx*0.05:.2f}s')
    # #     plt.colorbar(img, ax=ax)

    #     ax = fig.add_subplot(122)
    #     img = ax.contour(X_grid, Y_grid, full_state_recon[:x_pts*y_pts, idx].reshape(x_pts, y_pts), 20)
    #     ax.set(xlabel = 'x', ylabel = 'y', title = f'Learned quadratic emb. at time: {idx*0.05:.2f}s')
    #     plt.colorbar(img, ax=ax)

    #     fig.savefig(ROOT_PATH + '/simulation/'+ f"RD_model_testing_2_{idx:04d}.png", bbox_inches = 'tight', pad_inches = 0, , dpi = 300)
    #     plt.close(fig)
