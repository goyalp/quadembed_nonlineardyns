#!/bin/bash

# source /scratch/Goyalp/miniconda3/etc/profile.d/conda.sh

source /Users/goyalp/anaconda3/etc/profile.d/conda.sh

conda activate quad_embed

echo "##################################################################"
echo "############# Running Pendulum example ##############"
echo "##################################################################"

python Example_Pendulum.py

echo "##################################################################"
echo "############# Running MM example  ######"
echo "##################################################################"

python Example_MM.py

echo "##################################################################"
echo "############# Running Reaction-Diffusion example ##############"
echo "##################################################################"

python Example_ReactionDiffusion.py