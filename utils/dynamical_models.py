#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np


def mm_kinetics(x: float, t: float) -> float :
    """ This defines Miches-Menten Model.

    Args:
        x (float): state variable
        t (float): time instance

    Returns:
        float: defines the rate of change of x (state variable)
    """
    return [
        ((0.6 - 3*x) / (1 + (10*x/3)))
    ]


def nonlinear_pendulum(x: float, t:float) -> float:
    """ This defines nonlinear damped pendulum dynamics.

    Args:
        x (float): state variable defining position and velocity
        t (float): time instance

    Returns:
        float: defines the rate of change of x (state variable)
    """
    return [
        x[1],
        -np.sin(x[0]) - 0.05*x[1],
        ]
        