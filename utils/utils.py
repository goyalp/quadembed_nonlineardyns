import os
import random

import matplotlib.pyplot as plt
import numpy as np
import torch

#import meta_modules
#import scipy.io.wavfile as wavfile
#import cmapy

def cond_mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def plot_dictionary(dictionary):
    fig = plt.figure(figsize=(10, 5))
    ax = fig.add_subplot(111)
    for key, values in dictionary.items():
        ax.semilogy(values, label=str(key))
    ax.legend()
    return fig, ax

def reproducibility_seed(seed:int) -> None:
    """ To set seed for random variables. """
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.enabled = False

def plot_loghist(x, bins):
    hist, bins = np.histogram(x, bins=bins)
    logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))
    fig = plt.figure(figsize=(6.5,5))
    ax = fig.add_subplot(111)
    ax.hist(x, bins=logbins)
    plt.xscale('log')
    

def kron_einsum_batched_1d(A: torch.Tensor, B: torch.Tensor) -> torch.Tensor:
    """
    Batched Version of Kronecker Products
    :param A: has shape (b, a)
    :param B: has shape (b, k)
    :return: (b, ak)
    """
    res = torch.einsum('ba,bk->bak', A, B).view(A.size(0), A.size(1) * B.size(1))
    return res


def kron_einsum_batched_2d(A: torch.Tensor, B: torch.Tensor) -> torch.Tensor:
    """
    Batched Version of Kronecker Products
    :param A: has shape (b, c, a)
    :param B: has shape (b, c, k)
    :return: (b, c, ak)
    """
    res = torch.einsum('bca,bck->bcak', A, B).view(A.size(0), A.size(1), A.size(2) * B.size(2))
    return res


def dot_product(A: torch.Tensor, B: torch.Tensor) -> torch.Tensor:
    """
    Batched Version of Kronecker Products
    :param A: has shape (b, a)
    :param B: has shape (b, k)
    :return: (b,1)
    """
    if not (A.shape == B.shape):
        raise ValueError('Shape of A and B are not matching!')

    res = torch.einsum('ij,ij->i', A, B).unsqueeze(dim=1)
    return res

def save_parameters_configs(parameters, path):
    with open(path, "w+") as f:
        tmp = [
            str((key, value))
            for key, value in parameters.__dict__.items()
            if key[:2] != "__"
        ]
        tmp_str = "\n".join(tmp)
        f.write(tmp_str)


# Defining a quadratic model right-hand side
def quad_model(x, t, A, H, B):
    return A @ x + H @ (np.kron(x, x)) + B