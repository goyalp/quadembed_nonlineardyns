
def function_mse(model_output, gt):
    return {'func_loss': ((model_output['model_out'] - gt['func']) ** 2).mean()}


def mse_loss(model_output, true_output):
    return ((model_output - true_output)**2).mean()

