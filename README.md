# Generalized Quadratic-Embeddings for Nonlinear Dynamics using Deep Learning

This repository contains the Python implementation using the PyTorch framework to learn generalized quadratic-embeddings for nonlinear dynamics using deep learning time-series data. For this, we focus on learning a particular embedding such that a quadratic model can defiine. The methodology is depicted in the figure below.

<p align="center">
<img src="img/methodology.png"
     alt=""
     width="700"/>
</p>
     
<p align="center">
<img src="img/animated.gif" width="200" height="150">

</p>


The important steps of the methodology are:

1. Collect measurement data.
2. Estimate derivative or assume it to be given.
3. Define a neural network that learn a lifted variable, and a quadratic model defining its dynamics.
4. Train the network and model simultaneously 
 	

## Contains
* `Functions` contains externalized Python routine, e.g., training.
* `torchmeta` contains routines for defining networks. All these routines are directly borrowed from [https://github.com/vsitzmann/siren](https://github.com/vsitzmann/siren).  Moreover, the file `modules.py` are taken from [https://github.com/vsitzmann/siren/blob/master/modules.py](https://github.com/vsitzmann/siren/blob/master/modules.py) and modified accoringly. We have put the corresponding LICENSE file for the usage of the files in this folder. For this, we express our thanks to the authors [2].
* `Examples` contains all the examples presented in the paper [1]. 

## Reproducibility 
To reproduce all the results in the paper [1], please run `./Experiments/run_examples.sh` All the results will be saved in the folder ```.\results\```. Note that in the current setting, the trained models which are saved in the folder ```examples/Trained_models/``` will be loaded and figures will be generated. However, if you would like to train the models yourself, please set ```training_mode``` to ```True``` in the parameter class. 

## Dependencies
For reproducibility, we have stored all dependencies and their versions in `environment_quad_embed.yml`. A virtual environment, namely `quad_embed` can be created using `conda`:
 
 ```
 conda env create -f environment_quad_embed.yml
 conda activate quad_embed
 ``` 

## License
See the [LICENSE](LICENSE) file for license rights and limitations (MIT). 

## References
[1]. P. Goyal, and P. Benner, Generalized Quadratic-Embeddings forNonlinear Dynamics using Deep Learning, arXiv:xxxx.xxxx, 2022.

[2]. V. Sitzmann, N. P. J. Martel, A. W. Bergman, D. B. Lindell, G. Wetzstein, [Implicit Neural Representations with Periodic Activation Functions](https://proceedings.neurips.cc/paper/2020/file/53c04118df112c13a8c34b38343b9c10-Paper.pdf). In Proc. Inter. Conf. 33rd Conf. on Neural Information Processing Systems, vol. 33, 2020.

[4]. P., Adam et al., [PyTorch: An Imperative Style, High-Performance Deep Learning Library](http://papers.neurips.cc/paper/9015-pytorch-an-imperative-style-high-performance-deep-learning-library.pdf). Advances in Neural Information Processing Systems, pp. 8024--8035, 2019.

## Contact
For further queries, please contact the authors.




